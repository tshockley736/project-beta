import React, { useState } from 'react';


function ManufacturerForm() {
    const [submitted, setSubmitted] = useState(false)
    const [formData, setFormData] = useState({
        name: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8100/api/manufacturers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: { 'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setSubmitted(true)
            setFormData({
                name: '',
            });
        }
    }
    const handleFormChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const showMessage = submitted ? 'alert alert-success mt-4 offset-3 col-6' : 'alert alert-success d-none mb-0 offset-3 col-6';

    return (
      <>
        <div className={showMessage} id="form-success">
          Successfully Created a New Manufacturer
        </div>
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a New Manufacturer</h1>
              <form onSubmit={handleSubmit} id="create-manufacturer-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={formData.name} placeholder="Name" type="text" required name="name" id="name" className="form-control" />
                  <label htmlFor="first_name">Manufacturer</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </>
      );
}

export default ManufacturerForm
