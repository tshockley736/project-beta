import React, { useState, useEffect } from 'react';


function SalesForm(){
    const [salespeople, setSalespeople] = useState([])
    const [customers, setCustomers] = useState([])
    const [automobiles, setAutomobiles] = useState([])
    const [submitted, setSubmitted] = useState(false)
    const [formData, setFormData] = useState({
        price: '',
        salesperson:'',
        customer: '',
        automobile: '',
    })

    const getSalespeopleData = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salespeopleUrl);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }

    const getCustomerData = async () => {
        const customerUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(customerUrl);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    const getAutomobileData = async () => {
        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(automobileUrl);

        if (response.ok) {
            const data = await response.json();
            const availableAuto = []
            for (let auto of data.autos) {
                if (!auto.sold) {
                    availableAuto.push(auto)
                }
            }
            setAutomobiles(availableAuto);
            }
    }

    useEffect(() => {
        getSalespeopleData();
        getCustomerData();
        getAutomobileData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const salesUrl = 'http://localhost:8090/api/sales/';

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(salesUrl, fetchConfig);

        if (response.ok) {
            setSubmitted(true)
            const autoUrl = `http://localhost:8100/api/automobiles/${formData.automobile}/`

            const autoSold = {
                vin: formData.automobile,
                sold: true,
            }

            const fetchConfig = {
                method: "put",
                body: JSON.stringify(autoSold),
            }

            await fetch(autoUrl, fetchConfig);

            setFormData({
                price: '',
                salesperson:'',
                customer: '',
                automobile: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
          ...formData,
          [inputName]: value
        });
    }

    const showMessage = submitted ? 'alert alert-success mt-4 offset-3 col-6' : 'alert alert-success d-none mb-0 offset-3 col-6';

    return (
        <>
            <div className={showMessage} id="form-success">
                Successfully Recorded a New Sale
            </div>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Record a New Sale</h1>
                        <form onSubmit={handleSubmit} id="create-sales-form">
                                <div className="mb-3">
                                    <select onChange={handleFormChange} value={formData.automobile} required name="automobile" id="automobile" className="form-select">
                                        <option value="">Choose an Automobile VIN</option>
                                        {automobiles.map(automobile => {
                                        return (
                                        <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                        )
                                        })}
                                    </select>
                                </div>
                                <div className="mb-3">
                                    <select onChange={handleFormChange} value={formData.salesperson} required name="salesperson" id="salesperson" className="form-select">
                                        <option value="">Choose a Salesperson</option>
                                        {salespeople.map(salesperson => {
                                        return (
                                        <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                                        )
                                        })}
                                    </select>
                                </div>
                                <div className="mb-3">
                                    <select onChange={handleFormChange} value={formData.customer} required name="customer" id="customer" className="form-select">
                                        <option value="">Choose a Customer</option>
                                        {customers.map(customer => {
                                        return (
                                        <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                        )
                                        })}
                                    </select>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handleFormChange} value={formData.price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                                    <label htmlFor="price">Price</label>
                                </div>
                                <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
          );

}

export default SalesForm;
