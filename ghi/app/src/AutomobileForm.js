import React, { useState, useEffect } from 'react';


function AutomobileForm() {
    const [models, setModels] = useState([])
    const [error, setError] = useState(false)
    const [submitted, setSubmitted] = useState(false)
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    })

    const getModelData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }

    useEffect(() => {
        getModelData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const locationUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            setSubmitted(true)
            setError(false)
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            })
        } else{
            if (response.status === 400) {
                setError(true)
                setSubmitted(false)
            }
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const showMessage = submitted ? 'alert alert-success mt-4 offset-3 col-6' : 'alert alert-success d-none mb-0 offset-3 col-6';
    const showError = error ? 'alert alert-danger mb-0 mt-4 offset-3 col-6' : 'alert alert-danger d-none mb-0 offset-3 col-6';

    return (
        <>
            <div className={showMessage} id="form-success">
                Successfully Created a Automobile
            </div>
            <div className={showError} id="form-success">
                VIN Already Exists in our Database
            </div>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add an Automobile to Inventory</h1>
                        <form onSubmit={handleSubmit} id="create-automobile-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.year} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange}  value={formData.vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleFormChange} value={formData.model_id} required name="model_id" id="model_id" className="form-select">
                                    <option value="">Choose A Model</option>
                                    {models.map(model => {
                                        return (
                                            <option key={model.id} value={model.id}>{model.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default AutomobileForm;
