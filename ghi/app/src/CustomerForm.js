import React, { useState } from 'react';


function CustomerForm() {

    const [submitted, setSubmitted] = useState(false)
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address:'',
        phone_number: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8090/api/customers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: { 'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig);

         if (response.ok) {
            setSubmitted(true)
            setFormData({
                first_name: '',
                last_name: '',
                address:'',
                phone_number: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const showMessage = submitted ? 'alert alert-success mt-4 offset-3 col-6' : 'alert alert-success d-none mb-0 offset-3 col-6';

    return (
        <>
            <div className={showMessage} id="form-success">
                Successfully Added a New Customer
            </div>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a New Customer</h1>
                        <form onSubmit={handleSubmit} id="create-salesperson-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.first_name} placeholder="First Name" type="text" required name="first_name" id="first_name" className="form-control" />
                                <label htmlFor="first_name">First Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.last_name} placeholder="Last Name" required name="last_name" type="text" id="last_name" className="form-control" />
                                <label htmlFor="last_name">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.address} placeholder="Address" id="address" required name="address" className="form-control"  />
                                <label htmlFor="address">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.phone_number} placeholder="Phone Number" id="phone_number" required name="phone_number" className="form-control"  />
                                <label htmlFor="phone_number">Phone Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
      );
}

export default CustomerForm;
